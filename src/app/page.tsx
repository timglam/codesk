"use client";

import 'ace-builds/src-noconflict/ace';
import "ace-builds/src-noconflict/ext-language_tools";
import "ace-builds/src-noconflict/mode-css";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-chrome";
import "ace-builds/src-noconflict/theme-dracula";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/theme-tomorrow";
import "ace-builds/src-noconflict/theme-twilight";
import { useEffect, useState } from "react";
import CodeEditor from "./components/CodeEditor";

import { twMerge } from "tailwind-merge";
import LivePreview from "./components/LivePreview";
import ResizeBar from "./components/ResizeBar";
import Button from "./components/ui/Button";

export default function Home() {
  const [htmlCode, setHtmlCode] = useState(
    localStorage.getItem("codesk-html") || ""
  );
  const [cssCode, setCssCode] = useState(
    localStorage.getItem("codesk-css") || ""
  );
  const [jsCode, setJsCode] = useState(localStorage.getItem("codesk-js") || "");
  const [hasUpdate, setHasUpdate] = useState(false);
  const [isResizing, setIsResizing] = useState(false);
  const [editorHeight, setEditorHeight] = useState("50%");
  const [theme, setTheme] = useState(
    localStorage.getItem("codesk-theme") || "monokai"
  );
  // const [collapsedEditors, setCollapsedEditors] = useState<string[]>([]);

  function handleChange(value: string, mode: string) {
    switch (mode) {
      case "html":
        setHtmlCode(value);
        break;
      case "css":
        setCssCode(value);
        break;
      case "js":
        setJsCode(value);
        break;
    }
    setHasUpdate(true);
  }

  function handleSave() {
    localStorage.setItem("codesk-html", htmlCode);
    localStorage.setItem("codesk-css", cssCode);
    localStorage.setItem("codesk-js", jsCode);

    setHasUpdate(false);
  }

  function handleChangeTheme(e: React.ChangeEvent<HTMLSelectElement>) {
    localStorage.setItem("codesk-theme", e.target.value);
    setTheme(e.target.value);
  }

  useEffect(() => {
    const handleMouseMove = (e: MouseEvent) => {
      if (!isResizing) return;
      const newHeight = e.pageY - 64 + "px";
      setEditorHeight(newHeight);
    };

    const handleMouseUp = () => {
      if (!isResizing) return;
      setIsResizing(false);
    };

    window.addEventListener("mousemove", handleMouseMove);
    window.addEventListener("mouseup", handleMouseUp);

    return () => {
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("mouseup", handleMouseUp);
    };
  }, [isResizing]);

  return (
    <div
      className={twMerge("flex flex-col h-screen", isResizing && "select-none")}
    >
      <div className="flex items-center justify-between bg-gray-900 text-white px-2 py-3">
        <h1 className="text-2xl">
          <span className="text-slate-500 select-none">&lt;/&gt;{" "}</span>
          codesk
        </h1>

        <div className="flex gap-4 items-center">
          <select
            className="h-8 w-36 bg-slate-500 p-1 px-2 rounded-lg"
            onChange={handleChangeTheme}
            value={theme}
          >
            <optgroup label="Light">
              <option value="github">github</option>
              <option value="tomorrow">tomorrow</option>
              <option value="chrome">chrome</option>
            </optgroup>
            <optgroup label="Dark">
              <option value="monokai">monokai</option>
              <option value="dracula">dracula</option>
              <option value="twilight">twilight</option>
            </optgroup>
          </select>

          <Button
            onClick={handleSave}
            className={twMerge(
              hasUpdate && "bg-green-700 hover:bg-green-600 focus:bg-green-600"
            )}
          >
            Save
          </Button>
        </div>
      </div>
      <div className="flex w-full" style={{ height: editorHeight }}>
        <div className="flex-1">
          <CodeEditor
            mode="html"
            value={htmlCode}
            name="HTML"
            theme={theme}
            onChange={(value) => handleChange(value, "html")}
          />
        </div>
        <div className="flex-1">
          <CodeEditor
            mode="css"
            value={cssCode}
            name="CSS"
            theme={theme}
            onChange={(value) => handleChange(value, "css")}
          />
        </div>
        <div className="flex-1">
          <CodeEditor
            mode="javascript"
            value={jsCode}
            name="JS"
            theme={theme}
            onChange={(value) => handleChange(value, "js")}
          />
        </div>
      </div>

      <ResizeBar onMouseDown={() => setIsResizing(true)} />

      <div className="flex-1 z-10">
        <LivePreview html={htmlCode} css={cssCode} js={jsCode} />
      </div>
      {isResizing && (
        <div
          style={{
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: 9999,
            cursor: "row-resize",
          }}
        ></div>
      )}
    </div>
  );
};