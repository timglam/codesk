
type Props = {
  html: string;
  css: string;
  js: string;
}

function LivePreview(props: Props) {
  const cssString = "body{background:white;}" + props.css;
  const srcDoc = `
  <html>
    <body>${props.html}</body>
    <style>${cssString}</style>
    <script>${props.js}</script>
  </html>
`;
  return (
    <iframe
          srcDoc={srcDoc}
          title="Preview"
          sandbox="allow-scripts allow-modals"
          width="100%"
          height="100%"
        />
  )
}

export default LivePreview