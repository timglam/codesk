import AceEditor from 'react-ace';

type Props = {
  mode?: string;
  value?: string;
  name?: string;
  theme?: string;
  collapsed?: boolean;
  onChange?: ((value: string, event?: unknown) => void);
}

function CodeEditor(props: Props) {

  return (
    <>
    <div className="bg-slate-950 text-slate-400 text-xs p-1 select-none">
      <span>{props.name}</span>
    </div>
    <AceEditor 
      mode={props.mode}
      theme={props.theme}
      height='100%'
      onChange={props.onChange}
      value={props.value}
      name={props.name}
      editorProps={{ $blockScrolling: true }}
      setOptions={{
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: true,
        showLineNumbers: true,
        wrap: true,
        tabSize: 2,
        useWorker: false,
      }}
      style={{ width: '100%'}}
      />
    </>
  )
}

export default CodeEditor