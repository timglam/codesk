import { twMerge } from "tailwind-merge";
import { Resize } from "./icons/Resize";

type DivProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>;

type Props = DivProps & {
  // TODO: Add props
};

// React Div Element Props

function ResizeBar(props: Props) {
  const { className, ...divProps } = props;
  return (
    <div
      className={twMerge("w-full h-4 bg-gradient-to-t from-gray-950 to-gray-800 flex justify-center text-white cursor-row-resize z-10", className)}
      {...divProps}
    >
      <Resize className="w-4 h-4 scale-x-[2]" />
    </div>
  );
}

export default ResizeBar;
