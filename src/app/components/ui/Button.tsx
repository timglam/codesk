import React from 'react'
import { twMerge } from 'tailwind-merge';

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: React.ReactNode;
}

function Button(props: Props) {
  const { children, className, ...rest } = props
  return (
    <button className={twMerge("px-4 py-2 bg-slate-600 rounded-lg hover:bg-slate-700 focus:bg-slate-700 focus:ring-1 ring-slate-100 transition-shadow", className)} {...rest}>
      {children}
    </button>
  )
}

export default Button